package creations.caridad.com.android_sharedpreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    private String userName = "username";
    Context context;
    SharedPreferences sharedPreferences;

    //UI
    Button buttonSave;
    Button buttonLoad;
    Button buttonUpdate;
    Button buttonDelete;
    TextView textViewCurrentName;
    EditText editTextUserInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        sharedPreferences = this.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        // xml ui
        textViewCurrentName = (TextView) findViewById(R.id.textViewCurrentName);
        editTextUserInput = (EditText) findViewById(R.id.editTextEnterName);
        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonLoad = (Button) findViewById(R.id.buttonLoad);
        buttonUpdate = (Button) findViewById(R.id.buttonUpdate);
        buttonDelete = (Button) findViewById(R.id.buttonDelete);

        // check if shared preferences file is empty
        if (isStorageEmpty()) {
            Toast.makeText(this, "Try entering your name!", Toast.LENGTH_SHORT).show();
        } else {
            // load from username file
            textViewCurrentName.setText(sharedPreferences.getString(userName, ""));
        }

        // call methods here
        sharedPreferencesCrud();
    }

    private void sharedPreferencesCrud() {
        // SAVE
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if edittext is emtpy
                if (editTextUserInput.getText().toString().trim().length() != 0) {
                    //save
                    sharedPreferences.edit().putString(userName, editTextUserInput.getText().toString()).apply();
                    // add to textview
                    textViewCurrentName.setText(editTextUserInput.getText().toString());
                    // clear text
                    editTextUserInput.setText("");
                    Toast.makeText(context, "Name was saved!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Please enter a name", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // LOAD
        buttonLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if shared preferences file is empty
                if (isStorageEmpty()) {
                    Toast.makeText(context, "Nothing to load", Toast.LENGTH_SHORT).show();
                } else {
                    // load from username file
                    textViewCurrentName.setText(sharedPreferences.getString(userName, ""));
                    // clear text
                    editTextUserInput.setText("");
                    Toast.makeText(context, "Name is already loaded!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // UPDATE
        buttonUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // if false, return nothing to update
                if (isStorageEmpty()) {
                    Toast.makeText(context, "Nothing to update", Toast.LENGTH_SHORT).show();
                } else {
                    // check if edittext is emtpy
                    if (editTextUserInput.getText().toString().trim().length() != 0) {
                        //save
                        sharedPreferences.edit().putString(userName, editTextUserInput.getText().toString()).apply();
                        // add to textview
                        textViewCurrentName.setText(editTextUserInput.getText().toString());
                        // clear text
                        editTextUserInput.setText("");
                        Toast.makeText(context, "Name was updated!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "Please enter a name", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        // DELETE
        buttonDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isStorageEmpty()) {
                    Toast.makeText(context, "Nothing to delete!", Toast.LENGTH_SHORT).show();
                } else {
                    //delete
                    sharedPreferences.edit().clear().apply();
                    // add to textview
                    textViewCurrentName.setText("No name yet");
                    // clear text
                    editTextUserInput.setText("");
                }
            }
        });
    }

    private boolean isStorageEmpty() {
        // check if file is empty
        if (sharedPreferences.getString(userName, "").isEmpty()) {
            return true;
        } else {
            return false;
        }
    }
}
