# README #

A simple app demonstrating crud methods for android SharedPreferences.

### Features ###

* Save
* Load
* Update
* Delete

### Official Documentation ###
* [SharedPreferences](https://developer.android.com/training/basics/data-storage/shared-preferences.html)

![Screen Shot 2017-06-07 at 22.29.33.png](https://bitbucket.org/repo/akye7jG/images/2812948440-Screen%20Shot%202017-06-07%20at%2022.29.33.png)